.. _vbt:

=======================
Video BIOS Tables (VBT)
=======================

.. note::

   This document is a placeholder for updated content. The linked page may be
   outdated.

See `How to Dump Video BIOS`_.

.. _How to Dump Video BIOS: https://www.intel.com/content/www/us/en/docs/graphics-for-linux/developer-reference/1-0/dump-video-bios.html
