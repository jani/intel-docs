.. _how-to-build-the-kernel:

=======================
How to Build the Kernel
=======================

.. note::

   This document is a placeholder for updated content. The linked page may be
   outdated.

See `build guide`_.

.. _build guide: https://www.intel.com/content/www/us/en/docs/graphics-for-linux/developer-reference/1-0/build-guide.html
