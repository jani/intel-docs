.. _gpu-error-state:

==============================
How to Get the GPU Error State
==============================

.. note::

   This document is a placeholder for updated content. The linked page may be
   outdated.

See `How to Get the GPU Error State`_.

.. _How to Get the GPU Error State: https://www.intel.com/content/www/us/en/docs/graphics-for-linux/developer-reference/1-0/gpu-error-state.html
