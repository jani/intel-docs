.. _how-to-debug-suspend-resume-issues:

==================================
How to Debug Suspend-Resume Issues
==================================

.. note::

   This document is a placeholder for updated content. The linked page may be
   outdated.

See `How to Debug Suspend-Resume Issues`_.

.. _How to Debug Suspend-Resume Issues: https://www.intel.com/content/www/us/en/docs/graphics-for-linux/developer-reference/1-0/debug-suspend-resume.html
