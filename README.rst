====================================================
Developer Documentation for Intel Graphics for Linux
====================================================

This project hosts developer documentation for the i915 kernel driver, such as
bug filing and debugging guides.

See the deployed documentation at `Intel Graphics for Linux`_.

To contribute to the documentation, please use GitLab_ issues and merge
requests.

.. _Intel Graphics for Linux: https://drm.pages.freedesktop.org/intel-docs/

.. _GitLab: https://gitlab.freedesktop.org/drm/intel-docs
