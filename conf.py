# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Intel Graphics for Linux'
copyright = '2023, Intel Corporation'
author = 'Intel Corporation'
release = '1.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = []

templates_path = ['_templates']
exclude_patterns = ['README.rst', '_build', '.*']

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
html_static_path = ['_static']

# Theme options are theme-specific and customize the look and feel of a theme
# further.
#
# https://alabaster.readthedocs.io/en/latest/customization.html
html_theme_options = {
    'description': 'drm/i915 kernel driver',
    'extra_nav_links': {
        'i915 issues': 'https://gitlab.freedesktop.org/drm/intel/issues/',
        'drm-tip repo': 'http://cgit.freedesktop.org/drm-tip',
        'drm-intel repo': 'http://cgit.freedesktop.org/drm-intel',
        'intel-docs repo': 'https://gitlab.freedesktop.org/drm/intel-docs',
    }
}
